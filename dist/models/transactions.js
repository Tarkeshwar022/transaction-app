"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const transactionSchema = new mongoose_1.Schema({
    from: { type: String, required: true },
    to: { type: String, required: true },
    amount: { type: Number, required: true },
    date: { type: Date, default: Date.now }
});
const TransactionModel = (0, mongoose_1.model)('Transaction', transactionSchema);
// Static method to calculate the balance for a given address
transactionSchema.statics.getBalance = function (address) {
    return __awaiter(this, void 0, void 0, function* () {
        const sentAmount = yield TransactionModel.aggregate([
            { $match: { from: address } },
            { $group: { _id: null, total: { $sum: "$amount" } } }
        ]);
        const receivedAmount = yield TransactionModel.aggregate([
            { $match: { to: address } },
            { $group: { _id: null, total: { $sum: "$amount" } } }
        ]);
        const sent = sentAmount.length > 0 ? sentAmount[0].total : 0;
        const received = receivedAmount.length > 0 ? receivedAmount[0].total : 0;
        return received - sent;
    });
};
