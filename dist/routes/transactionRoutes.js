"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const transaction_1 = __importDefault(require("../models/transaction"));
const router = (0, express_1.Router)();
router.post('/addTransaction', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { from, to, amount } = req.body;
        // Perform some validation on from, to, and amount fields if needed.
        const transaction = new transaction_1.default({
            from,
            to,
            amount
        });
        const savedTransaction = yield transaction.save();
        res.json(savedTransaction);
    }
    catch (error) {
        res.status(500).json({ error: 'Failed to add transaction.' });
    }
}));
router.get('/getBalance/:address', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const address = req.params.address;
        // Perform some validation on the address field if needed.
        const balance = yield calculateBalance(address);
        res.json({ balance });
    }
    catch (error) {
        res.status(500).json({ error: 'Failed to get balance.' });
    }
}));
function calculateBalance(address) {
    return __awaiter(this, void 0, void 0, function* () {
        const sentAmount = yield transaction_1.default.aggregate([
            { $match: { from: address } },
            { $group: { _id: null, total: { $sum: "$amount" } } }
        ]);
        const receivedAmount = yield transaction_1.default.aggregate([
            { $match: { to: address } },
            { $group: { _id: null, total: { $sum: "$amount" } } }
        ]);
        const sent = sentAmount.length > 0 ? sentAmount[0].total : 0;
        const received = receivedAmount.length > 0 ? receivedAmount[0].total : 0;
        return received - sent;
    });
}
router.get('/transactionHistory/:address', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const address = req.params.address;
        // Perform some validation on the address field if needed.
        const transactionHistory = yield transaction_1.default.find({
            $or: [{ from: address }, { to: address }]
        }).sort({ date: -1 });
        res.json(transactionHistory);
    }
    catch (error) {
        res.status(500).json({ error: 'Failed to get transaction history.' });
    }
}));
exports.default = router;
