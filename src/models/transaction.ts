import { Schema, model, Document } from 'mongoose';

export interface Transaction extends Document {
  from: string;
  to: string;
  amount: number;
  date: Date;
}

const transactionSchema = new Schema<Transaction>({
  from: { type: String, required: true },
  to: { type: String, required: true },
  amount: { type: Number, required: true },
  date: { type: Date, default: Date.now }
});

export default model<Transaction>('Transaction', transactionSchema);

