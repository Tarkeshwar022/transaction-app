import express from 'express';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import transactionRoutes from './routes/transactionRoutes';

const app = express();
const PORT = 3000;

// Middleware
app.use(bodyParser.json());

// Connect to MongoDB
const MONGODB_URI = 'mongodb://localhost:27017/transaction_db'; // Replace with your MongoDB connection string
mongoose.connect(MONGODB_URI, {
  //useNewUrlParser: true,
//   useUnifiedTopology: true,
//   useCreateIndex: true,
//   useFindAndModify: false,
});

// Routes
app.use('/api', transactionRoutes);

// Error handling middleware
app.use((err: Error, req: express.Request, res: express.Response, next: express.NextFunction) => {
  console.error(err.stack);
  res.status(500).json({ error: 'Something went wrong!' });
});
// Start the server
app.listen(PORT, () => {
    console.log(`Server is running on http://localhost:${PORT}`);
  });