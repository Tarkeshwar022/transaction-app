import { Router, Request, Response } from 'express';
import Transaction, { Transaction as TransactionType } from '../models/transaction';


const router = Router();

router.post('/addTransaction', async (req: Request, res: Response) => {
    try {
      const { from, to, amount } = req.body;
  
      // Perform some validation on from, to, and amount fields if needed.
  
      const transaction: TransactionType = new Transaction({
        from,
        to,
        amount
      });
  
      const savedTransaction = await transaction.save();
      res.json(savedTransaction);
    } catch (error) {
      res.status(500).json({ error: 'Failed to add transaction.' });
    }
  });

router.get('/getBalance/:address', async (req: Request, res: Response) => {
    try {
        const address = req.params.address;

        // Perform some validation on the address field if needed.

        const balance = await calculateBalance(address);
        res.json({ balance });
    } catch (error) {
        res.status(500).json({ error: 'Failed to get balance.' });
    }
});
async function calculateBalance(address: string): Promise<number> {
    const sentAmount = await Transaction.aggregate([
        { $match: { from: address } },
        { $group: { _id: null, total: { $sum: "$amount" } } }
    ]);

    const receivedAmount = await Transaction.aggregate([
        { $match: { to: address } },
        { $group: { _id: null, total: { $sum: "$amount" } } }
    ]);

    const sent = sentAmount.length > 0 ? sentAmount[0].total : 0;
    const received = receivedAmount.length > 0 ? receivedAmount[0].total : 0;

    return received - sent;
}

router.get('/transactionHistory/:address', async (req: Request, res: Response) => {
    try {
      const address = req.params.address;
  
      // Perform some validation on the address field if needed.
  
      const transactionHistory = await Transaction.find({
        $or: [{ from: address }, { to: address }]
      }).sort({ date: -1 });
  
      res.json(transactionHistory);
    } catch (error) {
      res.status(500).json({ error: 'Failed to get transaction history.' });
    }
  });

export default router;
